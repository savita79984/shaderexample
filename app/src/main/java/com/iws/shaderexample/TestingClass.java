package com.iws.shaderexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by pc on 7/22/2016.
 */
public class TestingClass extends AppCompatActivity implements RecyclerViewAdapter.ClickListner {

    SessionManagement session;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter mAdapter;

    TextView textView;

    String[] data = {"1", "2", "1", "2", "1", "2", "1", "2", "1", "2", "1", "2", "1", "2", "1", "2"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing);

        session = new SessionManagement(this);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), session.getFontStyle(),this);
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new RecyclerViewAdapter(this, 2, data);

        mAdapter.setClickListner(this);
        recyclerView.setAdapter(mAdapter);
        GridLayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void itemClicked(View view, int position) {

    }

    @Override
    public void itemLongClick(View view, int position) {

    }
}
