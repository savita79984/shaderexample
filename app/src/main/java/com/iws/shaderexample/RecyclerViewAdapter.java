package com.iws.shaderexample;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by pc on 7/22/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private String[] data;
    private int[] Images_data;
    private Context context;
    int a;
    private View view;

    private ClickListner clickListner;


    public RecyclerViewAdapter(Context con, int a, String[] data) {
        this.context = con;
        this.a = a;
        this.data = data;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.textView.setText("" + data[position]);
//        holder.imageView.setImageResource(Images_data[position]);
    }

    public void setClickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView textView;
        CardView linear_row;
        ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.country_name);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            linear_row = (CardView) itemView.findViewById(R.id.card_view);
            linear_row.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (clickListner != null) {
                clickListner.itemClicked(v, getPosition());

            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (clickListner != null) {
                clickListner.itemLongClick(v, getPosition());

            }
            return false;
        }
    }

    public interface ClickListner {
        public void itemClicked(View view, int position);

        public void itemLongClick(View view, int position);
    }
}
