package com.iws.shaderexample;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rtugeek.android.colorseekbar.ColorSeekBar;

/**
 * Created by pc on 7/22/2016.
 */
public class HomeActivity extends AppCompatActivity {

    SessionManagement session;
    TextView text, text1, text2, text3, text4, text5, text6;
    Button done;
    ImageView circle_img;
    ColorSeekBar colorSeekBar, colorSeekBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        session = new SessionManagement(this);

//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), session.getFontStyle());
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
        session = new SessionManagement(this);
        text = (TextView) findViewById(R.id.text);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        text4 = (TextView) findViewById(R.id.text4);
        text5 = (TextView) findViewById(R.id.text5);
        text6 = (TextView) findViewById(R.id.text6);
        done = (Button) findViewById(R.id.done);
        circle_img = (ImageView) findViewById(R.id.circle_img);

        colorSeekBar = (ColorSeekBar) findViewById(R.id.colorSlider);
        colorSeekBar1 = (ColorSeekBar) findViewById(R.id.colorSlider1);

        colorSeekBar.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int colorBarValue, int alphaBarValue, int color) {
                text.setTextColor(color);
                GradientDrawable bgShape = (GradientDrawable) circle_img.getBackground();
                bgShape.setColor(color);
                session.setTextColor(color);
            }
        });

        colorSeekBar1.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int colorBarValue, int alphaBarValue, int color) {
                text.setBackgroundColor(color);
                session.setTextBackgroundColor(color);
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                setResult(RESULT_OK, intent);
                finish();

            }
        });

        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("bunga_melati_putih.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "painting-the-light.ttf");
                text.setTypeface(face);
//                view.invalidate();
            }
        });
        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("painting-the-light.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "painting-the-light.ttf");
                text.setTypeface(face);
                view.invalidate();
            }
        });
        text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("Roboto-Thin.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "Roboto-Thin.ttf");
                text.setTypeface(face);
                view.invalidate();
            }
        });
        text4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("Roboto-ThinItalic.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "Roboto-ThinItalic.ttf");
                text.setTypeface(face);
            }
        });
        text5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("atmostsphere.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "atmostsphere.ttf");
                text.setTypeface(face);
            }
        });

        text6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setFontStyle("timesbd.ttf");
                Typeface face = Typeface.createFromAsset(getAssets(), "timesbd.ttf");
                text.setTypeface(face);
            }
        });

//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), session.getFontStyle());
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
    }
}
