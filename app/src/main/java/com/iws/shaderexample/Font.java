//package com.iws.shaderexample;
//
//import android.content.Context;
//import android.graphics.Typeface;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
///**
// * Created by pc on 7/22/2016.
// */
//public class Font {
//
//    public static void setAllTextView(ViewGroup parent) {
//        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
//            final View child = parent.getChildAt(i);
//            if (child instanceof ViewGroup) {
//                setAllTextView((ViewGroup) child);
//            } else if (child instanceof TextView) {
//                ((TextView) child).setTypeface(getFont());
//            }
//        }
//    }
//
//    public static Typeface getFont() {
//        return Typeface.createFromAsset(getApplicationContext().getInstance().getAssets(), "fonts/whateverfont.ttf");
//    }
//}