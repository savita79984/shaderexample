package com.iws.shaderexample;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by pc on 7/22/2016.
 */
public class FontChangeCrawler {
    private Context context;
    SessionManagement session;
    private Typeface typeface;

    public FontChangeCrawler(Context context, Typeface typeface) {
        this.context = context;
        this.typeface = typeface;
    }

    public FontChangeCrawler(AssetManager assets, String assetsFontFileName, Context context) {
        this.context = context;
        typeface = Typeface.createFromAsset(assets, assetsFontFileName);
    }

    public void replaceFonts(ViewGroup viewTree) {
        session = new SessionManagement(context);
        View child;
        for (int i = 0; i < viewTree.getChildCount(); ++i) {
            child = viewTree.getChildAt(i);
            if (child instanceof ViewGroup) {
                // recursive call
                replaceFonts((ViewGroup) child);
            } else if (child instanceof TextView) {
                // base case
                ((TextView) child).setTypeface(typeface);
                ((TextView) child).setTextColor(session.getTextColor());
                ((TextView) child).setHintTextColor(session.getTextColor());
//                ((TextView) child).setBackgroundColor(session.getTextBackgroundColor());

            } else if (child instanceof Button) {
                ((Button) child).setTypeface(typeface);
                ((Button) child).setTextColor(session.getTextColor());
                ((Button) child).setHintTextColor(session.getTextColor());

            } else if (child instanceof EditText) {
                // base case
                ((EditText) child).setTypeface(typeface);
                ((EditText) child).setTextColor(session.getTextColor());
                ((EditText) child).setHintTextColor(session.getTextColor());
            }
        }
    }
}
