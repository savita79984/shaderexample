package com.iws.shaderexample;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SessionManagement session;
    Button hello;
    TextView text, list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setBackgroundDrawableResource(R.drawable.background);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        session = new SessionManagement(this);
        if (session.getFontStyle().isEmpty()) {
            session.setFontStyle("Roboto-Thin.ttf");
        }

        if (session.getTextColor() == 0) {
            session.setTextColor(Color.WHITE);
        }

        if (session.getTextBackgroundColor() == 0) {
            session.setTextColor(Color.WHITE);
        }

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), session.getFontStyle(), this);
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        hello = (Button) findViewById(R.id.hello);
        text = (TextView) findViewById(R.id.text);
        list = (TextView) findViewById(R.id.list);

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HorizontalRecyclerView.class);
                startActivity(intent);
            }
        });

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TestingClass.class);
                startActivity(intent);
            }
        });

        hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent, 0);
            }
        });

//        ViewGroup group = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);
//        Font.setAllTextView(group);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
//                onResume();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
    //    public void refresh(View view) {          //refresh is onClick name given to the button
//        onRestart();
//    }

//    @Override
//    protected void onResume() {
//
//        super.onResume();
//        this.onCreate(null);
//    }
}
