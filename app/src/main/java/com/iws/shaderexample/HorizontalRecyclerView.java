package com.iws.shaderexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by pc on 7/25/2016.
 */
public class HorizontalRecyclerView extends AppCompatActivity implements HorizontalRecyclerViewAdapter.ClickListner {
    SessionManagement session;
    LinearLayout layout;
    private RecyclerView recyclerView;
    private HorizontalRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    int[] data = {R.drawable.background, R.drawable.data, R.drawable.dth, R.drawable.electricity, R.drawable.background,
            R.drawable.background, R.drawable.background, R.drawable.background, R.drawable.background, R.drawable.background};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horizontal_recycler_view);
        session = new SessionManagement(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layout = (LinearLayout) findViewById(R.id.layout);

        mAdapter = new HorizontalRecyclerViewAdapter(this, data);

        mAdapter.setClickListner(this);
        recyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void itemClicked(View view, int position, ImageView imageView) {
        layout.setBackgroundResource(data[position]);
    }

    @Override
    public void itemLongClick(View view, int position) {

    }
}
