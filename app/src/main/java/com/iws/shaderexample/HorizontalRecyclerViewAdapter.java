package com.iws.shaderexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by pc on 7/25/2016.
 */
public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<HorizontalRecyclerViewAdapter.MyViewHolder> {

    private int[] data;

    private Context context;
    int a;
    private View view;

    private ClickListner clickListner;

    public HorizontalRecyclerViewAdapter(Context con, int[] data) {
        this.context = con;
        this.data = data;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        holder.textView.setText("" + data[position]);
        holder.imageView.setImageResource(data[position]);
    }

    public void setClickListner(ClickListner clickListner) {
        this.clickListner = clickListner;
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView textView;
        LinearLayout linear_row;
        ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img);
            linear_row = (LinearLayout) itemView.findViewById(R.id.linear_row);
            linear_row.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (clickListner != null) {
                clickListner.itemClicked(v, getPosition(),imageView);

            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (clickListner != null) {
                clickListner.itemLongClick(v, getPosition());

            }
            return false;
        }
    }

    public interface ClickListner {
        public void itemClicked(View view, int position,ImageView imageView);

        public void itemLongClick(View view, int position);
    }
}
