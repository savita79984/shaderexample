package com.iws.shaderexample;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pc on 7/22/2016.
 */
public class SessionManagement {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Learning";
    public static final String KEY_FONT_STYLE = "font_style";
    public static final String KEY_TEXT_COLOR = "text_color";
    public static final String KEY_TEXT_BACKGROUND_COLOR = "background_color";

    // Constructor
    public SessionManagement(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFontStyle(String fontStyle) {
        editor.putString(KEY_FONT_STYLE, fontStyle);
        editor.commit();
    }

    public void setTextColor(int text_color) {
        editor.putInt(KEY_TEXT_COLOR, text_color);
        editor.commit();
    }

    public void setTextBackgroundColor(int background_color) {
        editor.putInt(KEY_TEXT_BACKGROUND_COLOR, background_color);
        editor.commit();
    }

    public int getTextBackgroundColor() {
        return pref.getInt(KEY_TEXT_BACKGROUND_COLOR, 0);
    }

    public String getFontStyle() {
        return pref.getString(KEY_FONT_STYLE, "");
    }

    public int getTextColor() {
        return pref.getInt(KEY_TEXT_COLOR, 0);

    }

}
